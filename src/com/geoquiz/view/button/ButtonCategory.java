package com.geoquiz.view.button;

/**
 * The menu's buttons who a player can press to choose category and modality
 * game.
 */
public enum ButtonCategory {
    /**
     * Represents the category "Capitali".
     */
    CAPITAL,
    /**
     * Represents the category "Valute".
     */
    CURRENCY,
    /**
     * Represents the category "Typical dishes".
     */
    COOCKING,
    /**
     * Represents the category "Bandiere".
     */
    FLAG,
    /**
     * Represents the category "Monumenti".
     */
    MONUMENT,

}
