package com.geoquiz.view.button;

public enum GameDifficulty {
    /**
     * Represents the difficulty level "Easy".
     */
    EASY,
    /**
     * Represents the difficulty level "Medium".
     */
    MEDIUM,
    /**
     * Represents the difficulty level "Difficult".
     */
    DIFFICULT
}
