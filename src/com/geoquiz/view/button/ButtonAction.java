package com.geoquiz.view.button;

/**
 * The menu's buttons who a player can press.
 */
public enum ButtonAction {
    /**
     * Log in if user is authenticted.
     */
    LOGIN,
    /**
     * Possibility to create an account.
     */
    REGISTRATION,
    /**
     * Exit the game.
     */
    EXIT,
    /**
     * Go to scene before.
     */
    BACK,
    /**
     * Enter in category scene.
     */
    PLAY,
    /**
     * Enter in option scene.
     */
    OPTION,
    /**
     * Register a new accont.
     */
    SAVE,
    /**
     * Enter in ranking scene.
     */
    LEADERBOARD,
    /**
     * Enter in statistics scene.
     */
    STATISTIC,

    /**
     * Enter in instructions scene.
     */
    INSTRUCTION;

}
