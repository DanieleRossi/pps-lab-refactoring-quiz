package com.geoquiz.view.button;

public enum GameMode {
    /**
     * Represents the game mode "Classic".
     */
    CLASSIC,
    /**
     * Represents the game mode "Challenge".
     */
    CHALLENGE,
    /**
     * Represents the game mode "Training".
     */
    TRAINING;
}
