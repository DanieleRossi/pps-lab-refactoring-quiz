package com.geoquiz.view.menu;

import com.geoquiz.view.button.ButtonCategory;
import com.geoquiz.view.utility.Background;

import java.io.IOException;
import java.util.*;

import com.geoquiz.view.button.ButtonAction;
import com.geoquiz.view.button.MyButton;
import com.geoquiz.view.button.MyButtonFactory;
import com.geoquiz.view.label.MyLabel;
import com.geoquiz.view.label.MyLabelFactory;

import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import org.jetbrains.annotations.NotNull;

/**
 * The scene where user can choose game category.
 */
public class CategoryScene extends Scene {

    private static final double POS_1_X = 100;
    private static final double POS_1_Y = 450;
    private static final double POS_2_X = 250;
    private static final double POS_2_Y = 300;
    private static final double POS_X_BACK = 450;
    private static final double POS_Y_BACK = 600;
    private static final double BUTTON_WIDTH = 350;
    private static final Text BUTTON_PRESSED = new Text();
    private static final double USER_LABEL_FONT = 40;

    private final Pane panel = new Pane();

    private final HBox hbox = new HBox(10);
    private final HBox hbox2 = new HBox(10);
    private final VBox vbox = new VBox();
    private List<MyButton> myCategoryButtons= new ArrayList<>();
    private Stage mainStage;
    /**
     * @param mainStage
     *            the stage where the scene is called.
     */
    public CategoryScene(final Stage mainStage) {
        super(new StackPane(), mainStage.getWidth(), mainStage.getHeight());

        this.mainStage = mainStage;

        final MyButton back;

        final MyLabel userLabel = MyLabelFactory.createMyLabel("USER: " + LoginMenuScene.getUsername(), Color.BLACK,
                USER_LABEL_FONT);

        createMyCategoryButtons();
        setCategoryButtonsListener();
        back = MyButtonFactory.createMyButton(ButtonAction.BACK.toString(), Color.BLUE, BUTTON_WIDTH);

        setBoxTranslation(hbox, POS_1_X, POS_1_Y);
        setBoxTranslation(hbox2, POS_2_X, POS_2_Y);
        setBoxTranslation(vbox, POS_X_BACK, POS_Y_BACK);

        setBoxChildrens(hbox, Arrays.asList((Node) getCategoryButton(ButtonCategory.FLAG), (Node) getCategoryButton(ButtonCategory.CURRENCY), (Node) getCategoryButton(ButtonCategory.COOCKING)));
        setBoxChildrens(hbox2, Arrays.asList((Node) getCategoryButton(ButtonCategory.MONUMENT), (Node) getCategoryButton(ButtonCategory.CAPITAL)));
        setBoxChildrens(vbox, Arrays.asList((Node) back));


        ((Node) back).setOnMouseClicked(event -> {
            if (!MainWindow.isWavDisabled()) {
                MainWindow.playClick();
            }
            try {
                mainStage.setScene(new MainMenuScene(mainStage));
            } catch (IOException e) {
                e.printStackTrace();
            }
        });


        this.panel.getChildren().addAll(Background.getImage(), Background.createBackground(), hbox, hbox2, vbox,
                Background.getLogo(), (Node) userLabel);

        this.setRoot(this.panel);
    }

    private void createMyCategoryButtons(){
        for (ButtonCategory buttonCategory : ButtonCategory.values()) {
            myCategoryButtons.add(MyButtonFactory.createMyButton(buttonCategory.toString(), Color.BLUE, BUTTON_WIDTH));
        }
    }

    @NotNull
    private MyButton getCategoryButton(ButtonCategory category){
       return myCategoryButtons.stream().filter(b -> b.getText().equals(category.toString())).findFirst().get();
    }

    private void setCategoryButtonsListener(){
        myCategoryButtons.forEach(b -> ((Node)b).setOnMouseClicked(event -> {
            if (!MainWindow.isWavDisabled()) {
                MainWindow.playClick();
            }
            BUTTON_PRESSED.setText(b.getText());
            mainStage.setScene(new ModeScene(mainStage));
        }));
    }

    private void setBoxTranslation(Pane box, double xTranslation, double yTranslation)
    {
        box.setTranslateX(xTranslation);
        box.setTranslateY(yTranslation);
    }

    private void setBoxChildrens(Pane box, List<Node> childrens){
        box.getChildren().addAll(childrens);
    }

    public static String getCategoryPressed() {
       return BUTTON_PRESSED.getText();
    }

}
